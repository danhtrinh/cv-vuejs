import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import home from '@/components/home'
import Counter from '@/components/Counter'
import Movie from '@/components/Movie'
import Mansory from '@/components/Mansory'
import Slider from '@/components/Slider'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: HelloWorld
        },
        {
            path: '/slider',
            name: 'slider',
            component: home
        },
        {
            path: '/counter',
            name: 'Counter',
            component: Counter
        }, 
        {
            path: '/movie',
            name: 'Movie',
            component: Movie
        },
        {
            path: '/mansory',
            name: 'Mansory',
            component: Mansory
        },
        {
            path: '/slider-home',
            name: 'Slider',
            component: Slider
        }

    ],
    mode: 'history',
})
