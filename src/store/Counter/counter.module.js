const state = {
    number: 0
}

// const getters = {
//     number: state => state.number * 2
// }

const actions = {
    increment({commit}) {
        commit('increment')
    },
    decrement({commit}) {
        commit('decrement')
    },
    reset({commit}) {
        commit('reset')
    }
}

const mutations = {
    increment: state => state.number++,
    decrement: state => state.number--,
    reset: state => {
        state.number = 0
        return state
    }
}

export const counter = {
    namespaced: true,
    state,
    //getters,
    actions,
    mutations
};