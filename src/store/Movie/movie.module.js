import { movieApi } from '../../api/movieApi'
import * as types from '../mutation-types'

const state = {
    dataMovies: {}
}

const actions = {
    async getMovie({dispatch, commit}) {
        const data = await movieApi.getAll()
        commit(types.GET_MOVIE_REQUEST, data)
        dispatch('counter/increment', { }, {root:true})
    },
    clear(context) {
        context.commit(types.CLEAR_REQUEST)
        context.dispatch('counter/reset', { }, {root:true})
    }
}

const mutations = {
    [types.GET_MOVIE_REQUEST]: (state, dataMovies) => {
        state.dataMovies = dataMovies
    },
    [types.CLEAR_REQUEST]: (state) => {
        state.dataMovies = {}
    }
}

export const movie = {
    namespaced: true,
    state,
    actions,
    mutations
};
