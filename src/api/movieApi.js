import axios from 'axios'
import MovieModel from './../models/movieModel'

export class movieApi {
    static getAll() { 
        return axios({
            method: 'get',
            url: 'https://facebook.github.io/react-native/movies.json',
            headers: {
                'Content-Type': 'application/json'
            }   
        }).then(response => {
            const movieModel = new MovieModel()
            const data = movieModel.getMovie(response.data)
            return data
        })
        .catch(error => {
            console.log(error)
            return false
        })
    } 
}